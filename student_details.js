//..............create database connection
const { Client } = require("pg");
var express = require("express");
var app = express();
const fs = require("fs");
var moment = require("moment")

var bodyParser = require("body-parser");
const { log, Console } = require("console");
app.use(express.json());

const clientt = new Client({
  user: "postgress",
  host: "localhost",
  database: "connection",
  password: "root",
});
module.exports = clientt;

//...............create server and client

app.listen(3200, () => {
  console.log("Sever is now listening at port 3300");
});

clientt.connect();

//...............insert data
app.post("/student_insert", async (req, res) => {
  // var sid = req.body.student_id;
  var sname = req.body.student_name;
  var sage = req.body.student_age;
  var sadr = req.body.student_address;

  console.log(sname, sage, sadr);
  var insertdata =
    'insert into "public"."student"(student_name,student_age,student_address)values($1,$2,$3)returning student_id';
  var studentdata = await clientt.query(insertdata, [sname, sage, sadr]);
  console.log(studentdata.rows[0]);

  var fkey = studentdata.rows[0];
  console.log(fkey.student_id);
  var sub1 = req.body.subject1;
  var sub2 = req.body.subject2;
  var sub3 = req.body.subject3;

  var insertsubdata =
    'insert into "public"."subject"(subject1,subject2,subject3,subject_student_id)values($1,$2,$3,$4)';
  var subjectdata = await clientt.query(insertsubdata, [
    sub1,
    sub2,
    sub3,
    fkey.student_id,
  ]);
  console.log(subjectdata);
  res.send({ result: true, message: "student detailstored successfully" });
});

app.post("/alldetail", async (req, res) => {
  var sname = req.body.s_name;
  var sage = req.body.s_age;
  var splace = req.body.s_place;
  var sdata =
    'insert into "public"."studentdata"(s_name,s_age,s_place)values($1,$2,$3)';
  var data = await clientt.query(sdata, [sname, sage, splace]);

  console.log(data);
  res.send({ result: true, message: "student detailstored successfully" });
});



//......using sname
app.post("/allusers", async (req, res) => {
  var sname = req.body.s_name;
  var d = 'SELECT * FROM "public"."studentdata" WHERE "s_name"=$1';
  var data = await clientt.query(d, [sname]);
  console.log(data);
  res.send({ result: true, message: "student detailstored successfully" });
});

//................full outer joint
app.post("/studentdetailedusers", async (req, res) => {
  var stuid = req.body.student_id;
  var d =
    "SELECT * FROM public.student INNER JOIN public.subject ON subject_id=subject_id WHERE student_id=$1 ";
  var data = await clientt.query(d, [j]);
  console.log(data);
  res.send({ result: true, message: "data retrived", data: data.rows });
});

// //.....create coloumn
// app.post("/datecolumn", async (req, res) => {
//   var tablename= req.body.tablename;
//   var d =
//     "ALTER TABLE public.student ADD COLUMN created_date1 DATE, updated_date1 DATE SELECT * FROM public.student value=$1;";
//     var data = await clientt.query(d, [tablename]);
//   console.log(data);
//   res.send({ result: true, message: "data retrived", data: data.rows });
// });

//.....insert date
app.post("/newdetail", async (req, res) => {
  var sname = req.body.student_name;
  var sage = req.body.student_age;
  var sadr = req.body.student_address;

  const CDate = new Date();
  var c = moment(CDate).format("YYYY-MM-DD ,HH:MM")
  console.log(c);
  var sdata =
    "INSERT into public.student(student_name,student_age,student_address,created_date,updated_date)VALUES($1,$2,$3,$4,$5)";
  var data = await clientt.query(sdata, [sname, sage, sadr, c, c]);
  res.send({ result: true, message: "student detailstored successfully" });
})

//...update date
app.post("/updatedd", async (req, res) => {
  
  var ssid= req.body.subject_student_id;
  var sub1 = req.body.subject1;
  var newdata = "UPDATE public.subject SET subject1=$1 WHERE subject_student_id=$2";

  var subjectdata = await clientt.query(newdata, [sub1,ssid]);
  console.log(subjectdata);

  if(subjectdata.rowCount>0)
  {
    
    const CDate = new Date();
    var c = moment(CDate).format("YYYY-MM-DD ,HH:MM")
    console.log(c);
    var newdata1 = "UPDATE public.student SET updated_date=$1 WHERE student_id=$2";
    var data1 = await clientt.query(newdata1, [c,ssid]);
    console.log(data1);
  }
  res.send({ result: true, message: "student detailstored successfully" });
});

//............delete
app.post("/deleterow", async (req, res) => {
  var id = req.body.student_id;


  var del= "delete FROM public.student WHERE student_id=$1";

  var empl = await clientt.query(del, [id]);
  console.log(empl);
  res.send("sucesss");
});
